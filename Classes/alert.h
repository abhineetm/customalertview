//
//  alert.h
//  tt
//
//  Created by Abhineet on 22/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface alert : NSObject {
	NSString *message;
	UIAlertView *alertView;
	UIActivityIndicatorView *spinner;
	UILabel *lbl;
}
-(id)init;
+(id)sharedAlertView;
-(void)showWithMessage;
-(void)setMessage:(NSString *)mm;


@end
