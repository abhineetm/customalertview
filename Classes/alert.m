//
//  alert.m
//  tt
//
//  Created by Abhineet on 22/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "alert.h"

@implementation alert
static alert *alrt;


-(id)init
{
	if(self == [super init])
	{
		
	}
		
	return self;
}

-(void)showWithMessage
{
	alertView = [[UIAlertView alloc] init];
	[alertView show];
	spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
	
	spinner.frame = CGRectMake(130, 20, spinner.frame.size.width, spinner.frame.size.height);
	[spinner startAnimating];
	[alertView addSubview:spinner];
	
	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, spinner.frame.origin.y+spinner.frame.size.height+10, 260, 30)];
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.text =@"Sharing";
	lbl.shadowColor = [UIColor clearColor];
	
	alertView.frame = CGRectMake(alertView.frame.origin.x, alertView.frame.origin.y, alertView.frame.size.width, alertView.frame.size.height+40);
	
	[alertView addSubview:lbl];
}

-(void)setMessage:(NSString *)mm
{
	lbl.text = mm;
}

-(void)dissmiss
{
	[alertView dismissWithClickedButtonIndex:0 animated:YES];
}
+(id)sharedAlertView
{
		if(alrt == nil)
			alrt = [[alert alloc] init];
	return alrt;
}

@end
