//
//  ttAppDelegate.m
//  tt
//
//  Created by Abhineet on 22/09/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "ttAppDelegate.h"
#import "ttViewController.h"

@implementation ttAppDelegate

@synthesize window;
@synthesize viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
	
	return YES;
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
