//
//  ttAppDelegate.h
//  tt
//
//  Created by Abhineet on 22/09/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ttViewController;

@interface ttAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ttViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ttViewController *viewController;

@end

